# JER tables

Download tables from [JetMET GitHub](https://github.com/cms-jet/JRDatabase/tree/master/textFiles) using `getJERtables`.

*Warning:* we do no recommend that you download the whole repository...

In general, to choose the right tables, follow the [JetMET recommendation](https://cms-jerc.web.cern.ch/Recommendations/).
