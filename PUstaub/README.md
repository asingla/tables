# PU staub tables

These files are produced by the DAS machinery exclusively. For data sets in bins of the hard scale, use `getPUstaubLimitsSlices`; for flat data sets, use `getPUstaubLimitsFlat`.
