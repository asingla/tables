# PU cleaning tables

The tables contain the largest value of transverse momentum at generator level per event ID in the Minimum Bias sample. It can be used as argument to the `applyPUcleaning` command.

To obtain such tables, produce n-tuples from the MinBias sample that was used to simulate the pileup of the samples of interest, then use `getHighScalePUeventIDs` on the raw ntuples.

To find the right MinBias sample, find a PPD expert to guide you through the follow recipe: 
- check the *Chained Request* and find the *DRPremix* request;
- the Premix (a NeutrinoGun sample) is then given by the so-called `pileup_dataset` property of that request;
- repeat the two first steps with the Premix to find the MinBias sample.

Note: for the UL18 jet data, the tables were produced with command `getHighScalePUeventIDs` with 9 as threshold. A cut of `3*pthat` (`2*pthat`) was used in `getHighScalePUeventIDs.cc` (`applyPUcleaning.cc`, only if the rec jet pT is larger than 156 GeV---this is subject to changes).

## Large file storage

The tables being rather large, please use git LFS to handle them. Here is a brief recipe on how to proceed:
- The very first time, run `git lfs install` (do this once per local repo).
- Before adding a new file, use `git lfs track [file]` (do this once per file).
- Then you may add the file as usual with `git add [file]` (do this each time you change the file).
