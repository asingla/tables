# JES tables

Download tables from [JetMET GitHub](https://github.com/cms-jet/JECDatabase/tree/master/textFiles) using `getJEStables`.

*Warning:* we do no recommend that you download the whole repository...

In general, to choose the right tables, follow the [JetMET recommendation](https://cms-jerc.web.cern.ch/Recommendations/).
