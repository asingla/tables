# Jet veto maps

The maps provided by JetMET via [GitHub](https://github.com/cms-jet/JECDatabase) have a somewhat arbitrary bin content (at least from the user point of view). After downloading them (by hand or using the provided scripts), one should transform them into efficiency maps, e.g. using `getConservativeMap` (either by hand, or using the same script as for downloading).

See [Recommendations](https://cms-jerc.web.cern.ch/Recommendations/#jet-veto-maps).
