# Prefiring maps

See [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/L1ECALPrefiringWeightRecipe) and [personal homepage](https://lathomas.web.cern.ch/lathomas/TSGStuff/L1Prefiring/PrefiringMaps_2016and2017/) by Laurent Thomas.
Two versions exist:
 - A version with the correction factors integrated over the year for 2016 and for 2017 separately, with finer binning: `L1prefiring_jetpt_201*.root".
 - Another version with the correction factors per era in 2016 and 2017 in one single file, with coarser binning: `JetPrefiringMapsperIOV\_EOY.root`.

Unfortunately, the files must be downloaded by hand.

At the moment, DAS only includes jets, but one should consider photon and muon maps as well.

Reminder: the prefiring effect only applies to 2016 and 2017 (one run in 2018 seems to be affected as well, but that's all).

![Average map for jet prefiring in 2016](L1prefiring_jetpt_2016BtoH.pdf)
![Average map for jet prefiring in 2017](L1prefiring_jetpt_2017BtoF.pdf)

![Average map for photon prefiring in 2016](L1prefiring_photonpt_2016BtoH.pdf)
![Average map for photon prefiring in 2017](L1prefiring_photonpt_2017BtoF.pdf)
